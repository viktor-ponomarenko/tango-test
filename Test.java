import java.util.*;

/**
 * Implement a function that will get a pointer to a one-way-linked list head 
 * and return an array of two linked lists.
 * First, of them, they should contain only odd elements from the input, while 
 * the second only even elements. Both returned lists should be in a reversed order
 */
class Test {
  private static final int MAX_INT = 12;
  private static final int LIST_SIZE = 12;

  public static void main(String[] args) {
    Node head = generateLinkedList();
    printList(head);
    Node[] lists = getLinkedLists(head);
    printList(lists[0]);
    printList(lists[1]);
  }
  
  private static void printList(Node list) {
    Node next = list;
    final StringBuilder toLog = new StringBuilder();
    while (next != null) {
      if (toLog.length() > 0) {
        toLog.append(",");
      }
      toLog.append(next.getValue());
      next = next.getNext();
    }
    toLog.insert(0, "[").append("]");
    System.out.println(toLog);
  }

  private static Node[] getLinkedLists(Node head) {
    final List<Integer> items = new ArrayList<Integer>();
    Node next = head;
    while (next != null) {
      items.add(next.getValue());
      next = next.getNext();
    }
    Collections.sort(items, Collections.reverseOrder());
    Node evenHead = null;
    Node even = null;
    Node oddHead = null;
    Node odd = null;
    for (int value : items) {
      if (value % 2 == 0) {
        Node newEven = new Node(value);
        if (even != null) {
          even.setNext(newEven);
        } else {
          evenHead = newEven;
        }
        even = newEven;
      } else {
        Node newOdd = new Node(value);
        if (odd != null) {
          odd.setNext(newOdd);
        } else {
          oddHead = newOdd;
        }
        odd = newOdd;
      }
    }
    return new Node[]{evenHead, oddHead};
  }

  private static Node generateLinkedList() {
    Random random = new Random();
    Node head = new Node(random.nextInt(MAX_INT));
    Node lastNode = head;
    for (int index = 0; index < LIST_SIZE - 1; index++) {
      Node node = new Node(random.nextInt(MAX_INT));
      lastNode.setNext(node);
      lastNode = node;
    }
    return head;
  }
}
