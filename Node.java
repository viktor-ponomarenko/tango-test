/**
 * Simple implementation of linked list
 */
class Node implements Comparable<Node> {
  private int value = 0;
  private Node next;

  Node(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public Node getNext() {
    return next;
  }

  public void setNext(Node node) {
    this.next = node;
  }

  @Override
  public int compareTo(Node node) {
    if (this.value == node.getValue()) {
      return 0;
    }
    return this.value - node.getValue();
  }
}